package uk.ac.ucl.cs.mr.statnlpbook.assignment3
import java.io._
/**
 * @author rockt
 */
object Main extends App {
  /**
   * Example training of a model
   *
   * Problems 2/3/4: perform a grid search over the parameters below
   */
  val learningRate = 0.003
  val vectorRegularizationStrength = 0.1
  val matrixRegularizationStrength = 0.001
  val wordDim = 15
  val hiddenDim = 10

  val trainSetName = "train"
  val validationSetName = "dev"

  val model: Model = new SumOfWordVectorsModel(wordDim, vectorRegularizationStrength)
 // val model: Model = new RecurrentNeuralNetworkModel(wordDim, hiddenDim, vectorRegularizationStrength, matrixRegularizationStrength)

  def epochHook(iter: Int, accLoss: Double): Unit = {
    println("Epoch %4d\tLoss %8.4f\tTrain Acc %4.2f\tDev Acc %4.2f".format(
      iter, accLoss, 100 * Evaluator(model, trainSetName), 100*Evaluator(model, validationSetName)))
  }

  StochasticGradientDescentLearner(model, trainSetName, 100, learningRate, epochHook)  // use 56 instead of 100 when predict test



  /**
   * Comment this in if you want to look at trained parameters
   */
  /*
  for ((paramName, paramBlock) <- model.vectorParams) {
    println(s"$paramName:\n${paramBlock.param}\n")
  }
  for ((paramName, paramBlock) <- model.matrixParams) {
    println(s"$paramName:\n${paramBlock.param}\n")
  }
  */

  // test prediction
 /* val fw = new PrintWriter(new File("./data/assignment3/predictions_own.txt"))
  val test_iterations = SentimentAnalysisCorpus.numExamples("test")
  for(i<- 0 until test_iterations){
    val (sentence, target) = SentimentAnalysisCorpus.getExample("test")
    if(model.predict(sentence)){
      fw.write("1" + sentence.mkString(" ") + "\n")
    }else{
      fw.write("0" + sentence.mkString(" ") + "\n")
    }
  }
  fw.close()*/

}