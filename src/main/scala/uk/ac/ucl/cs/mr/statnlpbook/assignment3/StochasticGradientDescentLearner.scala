package uk.ac.ucl.cs.mr.statnlpbook.assignment3
/**
 * Problem 2
 */
object StochasticGradientDescentLearner extends App {
  def apply(model: Model, corpus: String, maxEpochs: Int = 10, learningRate: Double, epochHook: (Int, Double) => Unit): Unit = {
    val iterations = SentimentAnalysisCorpus.numExamples(corpus)
    for (i <- 0 until maxEpochs) {
      var accLoss = 0.0
      for (j <- 0 until iterations) {
        if (j % 1000 == 0) print(s"Iter $j\r")
        val (sentence, target) = SentimentAnalysisCorpus.getExample(corpus)
        //todo: update the parameters of the model and accumulate the loss
        // drop-out input by 20%
       /* var new_sentence = Seq[String](sentence(0))
        if(sentence.length > 1){
          for (n<-1 until sentence.length){
            if(random.nextDouble() < 0.8){
              new_sentence = new_sentence :+ sentence(n)
            }
          }
        }
        val modelLoss = model.loss(new_sentence, target)*/

        // loss model
        val modelLoss = model.loss(sentence, target)

        // trigger all the forward and calculate loss for the sentence
        // accumulate the loss
        accLoss = accLoss + modelLoss.forward()

        // trigger all the backward and calculate gradients
        modelLoss.backward()

        modelLoss.update(learningRate)
      }
      epochHook(i, accLoss)
    }
  }
}